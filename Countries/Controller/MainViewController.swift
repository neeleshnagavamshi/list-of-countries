//
//  ViewController.swift
//  Countries
//
//  Created by Neelesh on 19/08/20.
//  Copyright © 2020 Neelesh. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var countriesTV: UITableView!
    @IBOutlet weak var countriesCV: UICollectionView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    private var viewModel = CountryViewModel()
    private var shared = CoreDataManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateViewOnLoad()
    }
    
    private func updateViewOnLoad() {
        
        self.showLoadingDialog(view: self.view)
        viewModel.getCountriesData()
        
        viewModel.receivedData = { [weak self] () in
            DispatchQueue.main.async {
                self?.countriesTV.reloadData()
                self?.countriesCV.reloadData()
                self?.countriesTV.isHidden = false
                self?.hideLoadingDialog(view: self!.view)
            }
        }
    }
    
    @IBAction func segmentedControl(_ sender: Any) {
        guard segmentedControl.selectedSegmentIndex == 0 else {
            self.countriesTV.isHidden = true
            self.countriesCV.isHidden = false
            
            return
        }
        self.countriesTV.isHidden = false
        self.countriesCV.isHidden = true
    }
    
    @IBAction func saveData(_ sender: Any) {
        
        self.showLoadingDialog(view: self.view)
        
        Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { (timer) in
            for data in self.viewModel.countries.result {
                self.shared.saveData(id: data.id!, sortname: data.sortname!, name: data.name!)
            }
            self.hideLoadingDialog(view: self.view)
            self.showAlertDialog(message: "Saved all the data into your device", buttonTitle: "Ok")
        }
    }
}

extension MainViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard viewModel.countries.success == true else {
            return 0
        }
        
        return viewModel.countries.result.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CountryTableCell", for: indexPath) as? CountryTableViewCell else {
            fatalError("Cell does not exists")
        }
        cell.countryLabel.text = "\(viewModel.countries.result[indexPath.row].id!). \(viewModel.countries.result[indexPath.row].name!)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard viewModel.countries.success == true else {
            return 0
        }
        
        return viewModel.countries.result.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CountryCollectionCell", for: indexPath) as? CountryCollectionViewCell else {
            fatalError("Cell does not exists")
        }
        cell.titleLabel.text = "\(viewModel.countries.result[indexPath.row].id!). \(viewModel.countries.result[indexPath.row].name!)"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 170, height: 60)
    }
}
