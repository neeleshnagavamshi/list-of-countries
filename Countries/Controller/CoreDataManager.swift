//
//  CoreDataManager.swift
//  Countries
//
//  Created by Neelesh on 11/09/20.
//  Copyright © 2020 Neelesh. All rights reserved.
//

import UIKit
import CoreData

class CoreDataManager: NSObject {
    
    private var appDelegate: AppDelegate!
    private var managedContext: NSManagedObjectContext!
    private var managedObject: NSManagedObject!
    private var entityDescription: NSEntityDescription!
    private var fetchRequest: NSFetchRequest<NSFetchRequestResult>!
    
    static let shared = CoreDataManager()
    
    private override init() {
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        managedContext = appDelegate.persistentContainer.viewContext
        entityDescription = NSEntityDescription.entity(forEntityName: "CountriesData", in: managedContext)
        fetchRequest = NSFetchRequest(entityName: "CountriesData")
    }
    
    func saveData(id: Int, sortname: String, name: String) {
        
        managedObject = NSManagedObject(entity: entityDescription, insertInto: managedContext)
        
        managedObject.setValue(id, forKey: "id")
        managedObject.setValue(sortname, forKey: "sortname")
        managedObject.setValue(name, forKey: "name")
        
        save()
    }
    
    private func save() {
        do {
            try managedContext.save()
        } catch {
            debugPrint("Unable to save data")
        }
    }
}
