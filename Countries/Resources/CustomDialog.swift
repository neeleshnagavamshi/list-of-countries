//
//  CustomActivityIndicator.swift
//  Countries
//
//  Created by Neelesh on 07/09/20.
//  Copyright © 2020 Neelesh. All rights reserved.
//

import UIKit

private var backgroundView: UIView!
private var activityIndicator: UIActivityIndicatorView!

extension UIViewController {
    
    func showLoadingDialog(view: UIView) {
        backgroundView = UIView()
        backgroundView.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        backgroundView.layer.cornerRadius = 8
        backgroundView.center = view.center
        
        activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        activityIndicator.center = CGPoint(x: backgroundView.frame.size.width / 2, y: backgroundView.frame.size.height / 2);
        activityIndicator.color = .white
        activityIndicator.startAnimating()
        
        backgroundView.addSubview(activityIndicator)
        view.addSubview(backgroundView)
    }
    
    func hideLoadingDialog(view: UIView) {
        activityIndicator.stopAnimating()
        backgroundView.removeFromSuperview()
    }
    
    func showAlertDialog(message: String, buttonTitle: String) {
        let alertDialog = UIAlertController(title: "Countries", message: message, preferredStyle: UIAlertController.Style.alert)
        present(alertDialog, animated: true, completion: nil)
        
        let okButton = UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default) { (action) in
            alertDialog.dismiss(animated: true, completion: nil)
        }
        alertDialog.addAction(okButton)
    }
}
